from typing import Any, List, Optional, Sequence, Tuple
from mysql import connector

class Database:
  def __init__(self, bot):
    self.logger = bot.logger.getChild("database")

    try:
      self.conn = connector.connect(
        host=bot.config.db_host,
        user=bot.config.db_user,
        password=bot.config.db_pass,
        database=bot.config.db_name)
      self.logger.info("Connected to MySQL Database")
    except connector.Error as e:
      self.logger.critical("Something went wrong while trying to connect to MySQL Database")
      self.logger.critical(e)
      exit()

  def initialize_tables(self):
    try:
      with open("database.sql", "r", encoding="utf-8") as file:
        self.advanced_query(file.read())
    except Exception as e:
      self.logger.critical("Something went wrong while trying to initialize the database tables")
      self.logger.critical(e)
      exit()

  def simple_query(self, query: str, args: Optional[Tuple[any]]) -> bool:
    with self.conn.cursor(dictionary=True) as cursor:
      cursor.execute(query, args)
      self.conn.commit()
      cursor.close()
      return True

  def advanced_query(self, file: str) -> int:
    with self.conn.cursor(dictionary=True) as cursor:
      statements = file.split(";")

      for statement in statements:
        if not statement.strip(): continue
        cursor.execute(statement)

      row_count = cursor.rowcount
      self.conn.commit()
      cursor.close()
      return row_count

  def insert_one(self, query: str, args: Optional[Tuple[any]]) -> Optional[int]:
    with self.conn.cursor(dictionary=True) as cursor:
      cursor.execute(query, args)
      last_id = cursor.lastrowid
      self.conn.commit()
      cursor.close()
      return last_id
  
  def insert_more(self, query: str, args: Optional[Tuple[any]]) -> int:
    with self.conn.cursor() as cursor:
      cursor.executemany(query, args)
      row_count = cursor.rowcount
      self.conn.commit()
      cursor.close()
      return row_count
  
  def update(self, query: str, args: Optional[Tuple[any]]) -> int:
    with self.conn.cursor(dictionary=True) as cursor:
      cursor.execute(query, args)
      row_count = cursor.rowcount
      self.conn.commit()
      cursor.close()
      return row_count
  
  def fetch_one(self, query: str, args: Optional[Tuple[any]]) -> Optional[Sequence[Any]]:
    with self.conn.cursor(dictionary=True) as cursor:
      cursor.execute(query, args)
      row = None
      
      if cursor.with_rows:
        row = cursor.fetchone()

      cursor.close()
      return row

  def fetch_more(self, query: str, args: Optional[Tuple[any]]) -> Sequence[Any] | List:
    with self.conn.cursor(dictionary=True) as cursor:
      cursor.execute(query, args)
      rows = []

      if cursor.with_rows:
        rows = cursor.fetchall()

      cursor.close()
      return rows