from discord.ext import commands
from typing import Optional

def user_only_cooldown(ctx: commands.Context) -> Optional[commands.Cooldown]:
  if ctx.author.id == ctx.bot.owner.id:
    return None
  if not ctx.bot.config.global_cooldown_enabled:
    return None
  return commands.Cooldown(1, ctx.bot.config.global_cooldown_duration)