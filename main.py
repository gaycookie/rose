import sys
from os import listdir
from config import Config
from logging import getLogger
from database import Database
from discord.ext import commands
from discord import Intents, User

sys.dont_write_bytecode = True
config = Config()

class PythonBot(commands.AutoShardedBot):
  def __init__(self, *, intents: Intents, config: Config):
    super().__init__(command_prefix=config.command_prefix, intents=intents)
    self.config = config

  async def setup_hook(self):
    self.logger = getLogger("discord.bot")
    self.database = Database(self)

    await load_cogs()
    self.database.initialize_tables()

    try:
      client.logger.info("Syncing %s commands with Discord...", len(self.tree._get_all_commands()))
      await self.tree.sync()
      client.logger.info("Commands successfully synced %s with Discord", len(self.tree._get_all_commands()))
    except Exception as e:
      client.logger.error("Error while trying to load sync commands: %s", e)

  @property
  def owner(self) -> User:
    return self.application.owner

client = PythonBot(intents=Intents.all(), config=config)

async def load_cogs():
  client.logger.info("Start loading cogs...")
  for file in filter(lambda file: file.endswith("py"), listdir('cogs')):
    try:
      await client.load_extension("cogs." + file[:-3])
    except Exception as e:
      client.logger.error("Error while trying to load cog: %s", "cogs." + file[:-3])
      client.logger.error(e)

@client.event
async def on_command_error(ctx: commands.Context, error: commands.CommandError):
  if isinstance(error, commands.CommandOnCooldown):
    return await ctx.send(f'Ahoy, matey! Take a breather, ye scallywag. Arrr!\n*{error}*', ephemeral=True)

  if isinstance(error, commands.NotOwner):
    return await ctx.send(f"Ahoy there, matey! Ye be not me captain, that be for sure. Arrr!\n*{error}*", ephemeral=True)

  if isinstance(error, commands.NoPrivateMessage):
    return await ctx.send(f"Ahoy, matey! This here task can only be accomplished aboard a public vessel. Arrr!\n*{error}*", ephemeral=True)

  if isinstance(error, commands.MissingPermissions):
    return await ctx.send(f"Ahoy, ye scallywag! Ye lack the authority to command me to carry out this deed. Arrr!\n*{error}*", ephemeral=True)

  await ctx.send("Avast, captain! The winds be foul and somethin' be gone horribly awry. A dire situation, indeed!", ephemeral=True)
  return client.logger.error(error)

@client.event
async def on_ready():
  client.logger.info("Logged in as %s", client.user.name)

if not config.token:
  sys.exit("No bot token was provided, exiting now!") 
client.run(token=client.config.token)