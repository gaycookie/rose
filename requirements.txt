mysql-connector-python==8.1.0
python-dotenv==1.0.0
discord.py==2.3.2
requests==2.31.0
langcodes==3.3.0
language_data==1.1