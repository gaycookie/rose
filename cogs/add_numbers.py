from discord import app_commands
from discord.ext import commands

class AddNumber(commands.Cog):
  def __init__(self, bot):
    self.bot = bot

  @commands.hybrid_command(name="add", with_app_command=True, description="Add the mother fucking numbers together.")
  @app_commands.describe(
    first_value='The first value you want to add something to',
    second_value='The value you want to add to the first value',
  )
  async def add(self, ctx, first_value: int, second_value: int):
    await ctx.send(f'{first_value} + {second_value} = {first_value + second_value}')

async def setup(bot):
  await bot.add_cog(AddNumber(bot))