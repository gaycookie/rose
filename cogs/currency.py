from typing import Optional
from discord.ext import commands
from discord import Embed, Message, User
from utils import user_only_cooldown

class Currency(commands.Cog):
  def __init__(self, bot):
    self.bot = bot
    self.logger = bot.logger.getChild("cog.currency")
    self.logger.info("Cog successfully loaded")

  @commands.Cog.listener()
  async def on_message(self, msg: Message):
    return

  @commands.hybrid_command(name="pouch", description="Pouch of Pieces")
  @commands.dynamic_cooldown(user_only_cooldown, commands.BucketType.user)
  async def pouch(self, ctx: commands.Context, user: Optional[User]):
    if not user:
      user = ctx.author
    
    pouches = self.bot.database.fetch_one("SELECT * FROM `pouches` WHERE `user_id` = %s;", (user.id,))

    if not pouches:
      self.bot.database.insert_one("INSERT INTO `pouches` (user_id) VALUES (%s);", (user.id,))
      pouches = self.bot.database.fetch_one("SELECT * FROM `pouches` WHERE `user_id` = %s;", (user.id,))
      print(pouches)

    embed = Embed(title="\💰 Pouch of Pieces")
    embed.set_author(name=user.name, icon_url=user.avatar.url)
    embed.add_field(name="Gold Coins", value="{:,}".format(pouches["coins"]).replace(",","."))
    embed.add_field(name="Doubloons", value="{:,}".format(pouches["doubloons"]).replace(",","."))

    await ctx.send(embed=embed)

  @commands.hybrid_command(name="daily", description="Claim your daily rewards")
  @commands.dynamic_cooldown(user_only_cooldown, commands.BucketType.user)
  async def daily(self, ctx: commands.Context):
    return

async def setup(bot):
  await bot.add_cog(Currency(bot))