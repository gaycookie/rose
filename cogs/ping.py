from discord.ext import commands

class Ping(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.hybrid_group(name="ping", with_app_command=True)
    async def ping(self, ctx):
      return

    @ping.command(name="text", with_app_command=True, description="Some text")
    async def text(self, ctx, text: str = ""):
       await ctx.send(f"Pong: {text}")

async def setup(bot):
  await bot.add_cog(Ping(bot))