from discord import User, app_commands, Embed
from discord.ext import commands
from utils import user_only_cooldown

class Mod(commands.Cog):
  def __init__(self, bot):
    self.bot = bot
    self.logger = bot.logger.getChild("cog.moderation")
    self.logger.info("Cog successfully loaded")

  @commands.hybrid_group(name="mod", with_app_command=True)
  async def main(self, ctx: commands.Context):
    return

  @main.command(name="prune", with_app_command=True, description="Prune messages from a text-channel")
  @commands.has_guild_permissions(manage_messages=True)
  @commands.dynamic_cooldown(user_only_cooldown, commands.BucketType.user)
  @app_commands.describe(count="Amount of messages to prune")
  async def prune(self, ctx: commands.Context, count: commands.Range[int, 1, 100]):
    messages = [message async for message in ctx.channel.history(limit=count, before=ctx.message.created_at)]
    reply = await ctx.reply(f"Ahoy, captain! I be settin' course to send {len(messages)} bottled messages to Davy Jones' locker. Aye aye!")
    async with ctx.channel.typing():
      await ctx.channel.delete_messages(messages=messages, reason=f"Pruned by {ctx.author.name}")
    await reply.edit(content=f"Ahoy, captain! I've sent {len(messages)} bottled messages to Davy Jones' locker, they be gone for good. Aye aye!")

  @main.group(name="warns", with_app_command=True)
  async def warns(self, ctx: commands.Context):
    return
  
  @warns.command(name="create", with_app_command=True, description="Create an user warning")
  @commands.has_guild_permissions(moderate_members=True)
  @commands.dynamic_cooldown(user_only_cooldown, commands.BucketType.user)
  @app_commands.describe(user="User to warn", reason="Reason for the warning")
  async def warns_create(self, ctx: commands.Context, user: User, reason: str):
    async with ctx.channel.typing():
      self.bot.database.insert_one("INSERT INTO warnings (guild_id, target_id, issuer_id, reason) VALUES (%s, %s, %s, %s);", (ctx.guild.id, user.id, ctx.author.id, reason))
      results = self.bot.database.fetch_more("SELECT * FROM `warnings` WHERE `guild_id` = %s AND `target_id` = %s ORDER BY `timestamp` DESC;", (ctx.guild.id, user.id))
      
      embed = Embed(colour=0xFF0000, timestamp=results[0]["timestamp"])
      embed.set_footer(text=f"Warnings on record: {len(results)}")
      embed.add_field(name="Reason", value=results[0]["reason"], inline=False)
      embed.add_field(name="Individual", value=f"{user.mention}", inline=True)
      embed.add_field(name="Issuer", value=f"{ctx.author.mention}", inline=True)
      embed.add_field(name="Case ID", value=f"{results[0]['case_id']}", inline=True)

      await ctx.send(f"**⚠️ {user.mention}, ye scoundrel! The tides of fate have dealt ye a warning. Take heed, or face the consequences! Arrr!**", embed=embed)

  @warns.command(name="lookup", with_app_command=True, description="Lookup warnings of an user")
  @commands.has_guild_permissions(moderate_members=True)
  @commands.dynamic_cooldown(user_only_cooldown, commands.BucketType.user)
  @app_commands.describe(user="User to lookup")
  async def warns_lookup(self, ctx: commands.Context, user: User):
    async with ctx.channel.typing():
      results = self.bot.database.fetch_more("SELECT * FROM `warnings` WHERE `guild_id` = %s AND `target_id` = %s ORDER BY `timestamp` DESC;", (ctx.guild.id, user.id))
      if len(results) == 0:
        return await ctx.send("Ahoy, captain! Me spyglass scanned the crew, but no warnings be sighted fer this soul. A clean slate, it be!", ephemeral=True)
      
      warnings_string = []
      for warning in results:
        warnings_string.append(f"[{warning['case_id']}]: {warning['reason']}")

      embed = Embed(colour=0xFF0000)
      embed.set_author(name=user.name, icon_url=user.avatar.url)
      embed.set_footer(text=f"Warnings on record: {len(results)}")
      embed.add_field(name="Warnings", value="\n".join(warnings_string), inline=False)

      await ctx.send(embed=embed)

  @warns.command(name="remove", with_app_command=True, description="Remove a warning by case")
  @commands.has_guild_permissions(moderate_members=True)
  @commands.dynamic_cooldown(user_only_cooldown, commands.BucketType.user)
  @app_commands.describe(case="Case ID to remove")
  async def warns_remove(self, ctx: commands.Context, case: str):
    async with ctx.channel.typing():
      results = self.bot.database.fetch_one("SELECT * FROM `warnings` WHERE `guild_id` = %s AND `case_id` = %s ORDER BY `timestamp` DESC;", (ctx.guild.id, case))
      if not results:
        return await ctx.send("Ahoy, captain! I've scoured the seas, but no warnings be found for this here 'Case ID'. All clear on the horizon!", ephemeral=True)
      
      self.bot.database.simple_query("DELETE FROM `warnings` WHERE `guild_id` = %s AND `case_id` = %s ORDER BY `timestamp` DESC;", (ctx.guild.id, case))
      await ctx.send(f"Ahoy, captain! I be pleased to report that I've successfully scrubbed away the warnin' ({results['case']}) from our records. Smooth sailin' ahead!")

  @warns.command(name="view", with_app_command=True, description="View a warning by case")
  @commands.has_guild_permissions(moderate_members=True)
  @commands.dynamic_cooldown(user_only_cooldown, commands.BucketType.user)
  @app_commands.describe(case="Case ID to view")
  async def warns_view(self, ctx: commands.Context, case: str):
    async with ctx.channel.typing():
      results = self.bot.database.fetch_one("SELECT * FROM `warnings` WHERE `guild_id` = %s AND `case_id` = %s;", (ctx.guild.id, case))
      if not results:
        return await ctx.send("Ahoy, captain! I've scoured the seas, but no warnings be found for this here 'Case ID'. All clear on the horizon!", ephemeral=True)

      count = self.bot.database.fetch_one("SELECT COUNT(*) as count FROM `warnings` WHERE `guild_id` = %s AND `target_id` = %s;", (ctx.guild.id, results["target_id"]))
      target = await ctx.bot.fetch_user(results["target_id"])
      issuer = await ctx.bot.fetch_user(results["issuer_id"])

      embed = Embed(colour=0xFF0000, timestamp=results["timestamp"])
      embed.set_footer(text=f"Warnings on record: {count['count']}")
      embed.add_field(name="Reason", value=results["reason"], inline=False)
      embed.add_field(name="Individual", value=f"{target.mention}", inline=True)
      embed.add_field(name="Issuer", value=f"{issuer.mention}", inline=True)
      embed.add_field(name="Case ID", value=f"{results['case_id']}", inline=True)

      await ctx.send(embed=embed)

async def setup(bot):
  await bot.add_cog(Mod(bot))