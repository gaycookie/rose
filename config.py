from os import environ
from typing import Optional
from dotenv import load_dotenv

load_dotenv()

class Config:
  def __init__(self) -> None:
    pass

  @property
  def token(self) -> Optional[str]:
    return environ.get("TOKEN", None)
  
  @property
  def db_host(self) -> str:
    return environ.get("DB_HOST", "localhost")
  
  @property
  def db_user(self) -> str:
    return environ.get("DB_USER", "root")
  
  @property
  def db_pass(self) -> str:
    return environ.get("DB_PASS", "password")
  
  @property
  def db_name(self) -> str:
    return environ.get("DB_NAME", "rose")
  
  @property
  def command_prefix(self) -> str:
    return environ.get("COMMAND_PREFIX", "[]")

  @property
  def global_cooldown_enabled(self) -> bool:
    return environ.get("GLOBAL_COOLDOWN_ENABLED", "False") == "True"
  
  @property
  def global_cooldown_duration(self) -> int:
    return int(environ.get("GLOBAL_COOLDOWN_DURATION", "2"))